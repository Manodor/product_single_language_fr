# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2010-2013 OpenERP s.a. (<http://openerp.com>).
#    Copyright (C) 2013 initOS GmbH & Co. KG (<http://www.initos.com>).
#    Author Thomas Rehn <thomas.rehn at initos.com>
#    Copyright (C) 2016 SARL SIRIUS-INFO (<http://www.sirius-info.fr>).
#    Author David Verove <contact@sirius-info.fr>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, api, fields
from openerp.osv import osv, orm
from openerp.tools.translate import _

import logging
_logger = logging.getLogger('__name__')

class ProductTemplate(osv.Model):
    _inherit = 'product.template'

    name = fields.Char('Titre', size=128, required=True, select=True, translate=False)
    description = fields.Text('Description', translate=False)
    description_purchase = fields.Text('Description', translate=False) 
    description_sale = fields.Text('Sale Description', translate=False)

    def _remove_translations(self, cr, uid, ids=None, context=None):
        """Remove translations from name and description field"""

        # all fields for which we remove the translation
        fields_to_translate = [
            'name',
            'description',
            'description_purchase',
            'description_sale'
        ]
        for field in fields_to_translate:
			# copy french translations into product.template 
            sql = """UPDATE product_template product
                        SET %(field)s =
                            COALESCE((
                                    SELECT value
                                      FROM ir_translation trans
                                     WHERE trans.lang = 'fr_FR'
                                       AND trans.name =
                                           'product.template,%(field)s'
                                       AND trans.type = 'model'
                                       AND product.id = trans.res_id
                                     ), %(field)s)""" % {'field': field}
            cr.execute(sql)
            # delete french translations
            sql = """DELETE FROM ir_translation
                           WHERE lang = 'fr_FR'
                             AND name = 'product.template,%(field)s'
                             AND type = 'model'""" % {'field': field}
            cr.execute(sql)
